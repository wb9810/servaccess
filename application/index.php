<?php
    /**
     * RaspAccess
     * Author: johnnyb (wb9801)
     * Version: 0.1
     */
     
    // start application session
    session_start();
    // load composer autoload
    require 'vendor/autoload.php';
    // load current config file    
    $config = new Config_Lite('./config/config.ini');
    // start redbean php
    use \RedBeanPHP\R as R;
    R::setup( 'mysql:host='.$config->get('db','server').';dbname='.$config->get('db','database').'',$config->get('db','user'), $config->get('db','password') );
    
    
    /**
     * Standard Routes
     * Updated: 17.10.2017
     * Author: johnnyb (wb9810)
     */
    Flight::route('/', function(){
        new SiteRender('logfile');
    });
    Flight::route('/logfile', function(){
        new SiteRender('logfile');
    });
    Flight::route('/access', function(){
        new SiteRender('access');
    });
    Flight::route('/server', function(){
        new SiteRender('server');
    });
    Flight::route('/settings', function(){
        new SiteRender('settings');
    });
    Flight::route('/logout', function(){
        new SiteRender('logout');
    });
    
    /**
     * API Routes V1
     * Updated: 17.10.2017
     * Author: johnnyb (wb9810)
     */
    Flight::route('/api/v1/@class/@function', function($class,$function) {
       $RequestData = (array)Flight::request()->data;
       $RequestData = array_pop($RequestData);
       $RequestMethod = Flight::request()->method;
       
       $NewRequest = new $class($RequestData);
       $Response = $NewRequest->$function();
       Flight::halt($Response['state'],$Response['msg']);
    });
    
    /**
     * Class Setup
     * Updated: 17.10.2017
     * Author: johnnyb (wb9810)
     */
    class setup {
        private $RequestData;
        private $ConfigData;
        public function __construct($RequestData) {
            $this->RequestData = $RequestData;
            $this->config_load();
        }
        public function adminpw() {
            if (password_verify($this->RequestData['passwordold'], $this->ConfigData->get('instance','password'))) {
                $GeneratedPassword = password_hash($this->RequestData['passwordnew'], PASSWORD_DEFAULT);
                $this->ConfigData->set('instance','password',$GeneratedPassword);
                $this->config_save();
                $response['state'] = 200;
                $response['msg'] = "Password changed successfully.";
            } else {
                $response['state'] = 400;
                $response['msg'] = "Your old password is wrong.";
            }
            return $response;
        }
        public function database() {
            $this->ConfigData->set('db','user',$this->RequestData['dbuser']);
            $this->ConfigData->set('db','password',$this->RequestData['dbpassword']);
            $this->config_save();
            $response['state'] = 200;
            $response['msg'] = "DB Config (re)created.";
            return $response;
        } 
        public function dbschema() {
            $conn = @ new mysqli($this->ConfigData->get('db','server'), $this->ConfigData->get('db','user'), $this->ConfigData->get('db','password'));
            // Check connection
            if ($conn->connect_error) {
                $response['state'] = 400;
                $response['msg'] = "Could not connect with your credentials.";
                return $response;
            } 
            // Create database
            $sql = "CREATE DATABASE ".$this->ConfigData->get('db','database');
            if ($conn->query($sql) === TRUE) {
                $response['state'] = 200;
                $response['msg'] = "Database created.";
            } else {
                $response['state'] = 400;
                $response['msg'] = "Could not create database (".$conn->error.").";
            }
            $conn->close();
            return $response;
        }
        public function service() {
            
        }
        private function config_load() {
            $this->ConfigData = new Config_Lite('./config/config.ini', LOCK_EX);
        }
        private function config_save() {
            $this->ConfigData->save();
        }
    }
    
    /**
     * Class Auth
     * Updated: 18.10.2017
     * Author: johnnyb (wb9810)
     */
    class auth {
        private $RequestData;
        private $Config;
        public function __construct($Postdata) {
            $this->RequestData = $Postdata;
            $this->Config = new Config_Lite('./config/config.ini');
        }
        public function signin() {
            if (password_verify($this->RequestData['password'], $this->Config->get('instance','password'))) {
                $_SESSION['logged_in'] = time();
                $response['state'] = 200;
                $response['msg'] = "Admin signed in - Welcome!";
            } else {
                $response['state'] = 400;
                $response['msg'] = "Sorry - wrong password.";
            }
            return $response;
        }
        public function signout() {
            session_destroy();
            $response['state'] = 200;
            $response['msg'] = "Session is closed - Bye!";
            return $response;
        }
    }
    
    /**
     * Class State
     * Updated: 20.10.2017
     * Author: johnnyb (wb9810)
     */
    class state {
        private $Config;
        public function __construct() {
            $this->Config = new Config_Lite('./config/config.ini');
        }
        public function database() {
            if(R::testConnection()) {
                return true;
            } else {
                return false; 
            }
        }
        public function service() {
            $ActualState = $this->CheckService();
            switch($ActualState) {
                case "ninstalled":
                    return false;
                    break;
                case "stopped":
                    return false;
                    break;
                case "started":
                    return true;
                    break;
                default:
                    return false;
                    break;
            }
        }
        public function CheckService() {
            $cmd = "systemctl is-active --quiet service && echo Service is running";
            $exec = shell_exec($cmd);
            if($exec == "Service is running") {
                    return "started";
            } else {
                return "ninstalled";
            }
        }
        public function server() {
            return false;
        }
    }
    
    /**
     * Class Site Render
     * Updated: 17.10.2017
     * Author: johnnyb (wb9810)
     */
    class SiteRender {
        private $SiteArray = array();
        public function __construct($CurrentSite) {
            $this->CheckState();
            if($this->CheckLogin()) {
                $this->SiteArray['SiteTitle'] = 'RASPaccess';
                $this->SiteArray['current_'.$CurrentSite] = 'active';
                if(file_exists(Flight::get('flight.views.path').'/view_'.$CurrentSite.'.php')) {
                    Flight::render('view_'.$CurrentSite, $this->SiteArray, 'body_content');
                } else {
                    Flight::render('temp_mod404', $this->SiteArray, 'body_content');
                }
                Flight::render('temp_header', $this->SiteArray, 'header_content');    
            } else {
                Flight::render('temp_auth', $this->SiteArray, 'body_content');
            }
            
            Flight::render('temp_layout');
        }
        private function CheckLogin() {
            if(isset($_SESSION['logged_in'])) {
                return true;
            } else {
                return false;
            }
        }
        private function CheckState() {
            $SiteState['errors'] = 0;
            $StateChecker = new state();
            if($StateChecker->database()) {
                $SiteState['db_ok'] = "border-success";
            } else {
                $SiteState['db_ok'] = "border-danger";
                $SiteState['errors']++;
            }
            if($StateChecker->service()) {
                $SiteState['service_ok'] = "border-success";
            } else {
                $SiteState['service_ok'] = "border-danger";
                $SiteState['errors']++;
            }
            if($StateChecker->server()) {
                $SiteState['server_ok'] = "border-success";
            } else {
                $SiteState['server_ok'] = "border-danger";
                $SiteState['errors']++;
            }
            $this->SiteArray['SiteState'] = $SiteState;
        }
    }
    
    /**
     * Class Access
     * Updated: 20.10.2017
     * Author: johnnyb (wb9810)
     */
    class access {
        private $RequestData;
        public function __construct($RequestData) {
            $this->RequestData = $RequestData;
        }
        public function all() {
            $acs = R::findAll( 'accesscards' );
            $out['data'] = array_values($acs);
            $response['state'] = 200;
            $response['msg'] = Flight::json($out);
            return $response;
        }
        public function add() {
            $acdb = R::find( 'accesscards', ' rfidtag = ? ', [ $this->RequestData['rfidtag'] ] );
            if($acdb) {
                $response['state'] = 500;
                $response['msg'] = "Tag already in database!";
            } else {
                $ac = R::dispense( 'accesscards' );
                    $ac->created = date("Y-m-d H:i:s");
                    $ac->rfidtag = $this->RequestData['rfidtag'];
                    $ac->comment = $this->RequestData['comment'];
                    $ac->active = true;
                    
                if(R::store($ac)) {
                    $response['state'] = 201;
                    $response['msg'] = "Accesscard created.";
                } else {
                    $response['state'] = 500;
                    $response['msg'] = "Database error!";
                }
            }
            return $response;
        }
        public function remove() {
            $ac = R::load( 'accesscards', $this->RequestData['id'] );
            $ac_exception = "";
            R::begin();
            try{
                R::trash( $ac );
                R::commit();
            }
            catch( Exception $e ) {
                $ac_exception = $e;
                R::rollback();
            }
            if(!$ac_exception) {
                $response['state'] = 200;
                $response['msg'] = "Accesscard deleted.";
            } else {
                $response['state'] = 500;
                $response['msg'] = "Database error! (".$ac_exception.")";
            }
            return $response;
        }
        public function update() {
            $ac = R::load( 'accesscards', $this->RequestData['id'] );
                $ac->active = $this->RequestData['active'];
                $ac->comment = $this->RequestData['comment'];
                $ac->updated = date("Y-m-d H:i:s");
            if(R::store($ac)) {
                $response['state'] = 200;
                $response['msg'] = "Accesscard updated.";
            } else {
                $response['state'] = 500;
                $response['msg'] = "Database error!";
            }
            return $response;
        }
        public function check() {
            $inputtag = $this->RequestData['tag'];
            $ac = R::find( 'accesscards', ' rfidtag = ? ', [ $inputtag ] );
            if($ac) {
                $ac = array_values($ac);
                if($ac[0]['active']) {
                    $response['state'] = 200;
                    $response['msg'] = "Access granted!";
                } else {
                    $response['state'] = 401;
                    $response['msg'] = "Access denied! Tag is inactive.";
                }
            } else {
                $response['state'] = 404;
                $response['msg'] = "Accesscard not found!";
            }
            $data = $response;
            $data['rfidtag'] = $inputtag;
            $data['rfidrange'] = 'local';
            
            $log = new accesslog();
            $log->createLog($data);
            
            return $response;
        }
    }
    
    /**
     * Class AccessLog
     * Updated: 22.11.2017
     * Author: johnnyb (wb9810)
     */
    class accesslog {
        public function all() {
            $log = R::findAll( 'accesslog' );
            $out['data'] = array_values($log);
            $response['state'] = 200;
            $response['msg'] = Flight::json($out);
            return $response;
        }
        public function createLog($data) {
            $log = R::dispense( 'accesslog' );
                $log->created = date("Y-m-d H:i:s");
                $log->code = $data['state'];
                $log->msg = $data['msg'];
                $log->rfidtag = $data['rfidtag'];
                $log->rfidrange = $data['rfidrange'];
            
            if(R::store($log)) {
                return true;
            } else {
                return false;
            } 
        }
    }

    
    R::close();
    Flight::start();
    