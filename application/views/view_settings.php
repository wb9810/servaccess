<?php
  $config = new Config_Lite('./config/config.ini');
?>
<div class="row">
    <div class="col-sm-6">
        <div class="card <?php echo $SiteState['service_ok'];?>">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-8">
                RASPaccess Service
              </div>
              <div class="col-sm-4 text-right">
                &nbsp;
              </div>
            </div>
                 
          </div>
          <div class="card-body" style="height: 250px;">
            <?php 
              $State = new state();
              $SiteState['service'] = $State->CheckService();
            ?>
            <?php if($SiteState['service'] == "ninstalled") : ?>
              <h4 class="card-title"><i class="mdi mdi-alert-circle text-danger" aria-hidden="true"></i> Service not installed</h4>
              <p class="card-text">The service does not seem to be installed. You can reinstall the service in the installation directory.</p>
              <p>Scriptpath (do it as sudo):<br><code>./installation/install_service.sh</code></p>
            
            <?php elseif($SiteState['service'] == "started") : ?>
              <h4 class="card-title"><i class="mdi mdi-check-circle text-success" aria-hidden="true"></i> Service is ready</h4>
              <p class="card-text">The service seems to be up and running.</p><br>
            <?php else : ?>
              <h4 class="card-title"><i class="mdi mdi-alert-circle text-danger" aria-hidden="true"></i> Systemerror</h4>
              <p class="card-text">We can't ready the active services on your PI.</p><br>
            <?php endif; ?>
          </div>
        </div>
    </div>
    <div class="col-sm-6">
      <div class="card">
          <div class="card-header">
            Change Password
          </div>
          <div class="card-body" style="height: 250px;">
            <form id="adminpw">
              <div class="form-group">
                <label for="exampleInputPassword1">Password old</label>
                <input type="password" name="passwordold" class="form-control" id="exampleInputPassword1" placeholder="Your current Password">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword2">Password new</label>
                <input type="password" name="passwordnew" class="form-control" id="exampleInputPassword2" placeholder="Your new Password">
              </div>
              <button type="submit" id="changepw" class="btn btn-secondary float-right">Save Password</button>
            </form>
          </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 20px;">
    <div class="col-sm-6">
        <div class="card <?php echo $SiteState['server_ok'];?>">
          <div class="card-header">
                SERVaccess Connection
          </div>
          <div class="card-body" style="height: 250px;">
            <h4><i class="mdi mdi-alert-circle text-danger" aria-hidden="true"></i> Module not built</h4>
            <p>SERVaccess is currently under development. Once the first executable version is deployed, we will unlock the API access.</p><p>Thank you for your patience!</p>
            <!--
            <?php if($SiteState['server_ok'] == 'border-danger') { ?>
              <h4><i class="mdi mdi-alert-circle text-danger" aria-hidden="true"></i> No API-Connection</h4>
              Try the following steps:
              <ul>
                <li>Re-enter and save the connection information.</li>
                <li>Check API key and RAPaccess door name.</li>
              </ul>
            <?php } else { ?>
              <h4 class="card-title"><i class="mdi mdi-check-circle text-success" aria-hidden="true"></i> API is answering</h4>
              Your SERVaccess instance is connected correctly and responds to the corresponding requests.<br>
            <?php } ?>
            <p class="text-right mb-5"><a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">Edit configuration</a></p>
            -->
          </div>
        </div>
    </div>
    <div class="col-sm-6">
      <div class="card <?php echo $SiteState['db_ok'];?>">
        <div class="card-header">
          Database Access
        </div>
        <div class="card-body" style="height: 250px;">
          <?php if($SiteState['db_ok'] == 'border-danger') { ?>
            <h4><i class="mdi mdi-alert-circle text-danger" aria-hidden="true"></i> Database error</h4>
            Try the following steps:
            <ul>
              <li>Re-enter and save the connection information.</li>
              <li>After you have saved it, you can re-create the schema using "create".</li>
            </ul>
          <?php } else { ?>
            <h4 class="card-title"><i class="mdi mdi-check-circle text-success" aria-hidden="true"></i> Database is ready</h4>
            <ul>
              <li>Your database is correctly installed and seems to work properly!</li>
              <li>Change configuration at this point at your own risk.</li>
            </ul><br>
          <?php } ?>
          <p class="text-right mb-5"><a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">Edit configuration</a></p>
        </div>
      </div>
    </div>
</div>

<!--
<div class="row" style="margin-top: 20px;">
    <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            Board Assignment
          </div>
          <div class="card-body">
            <table class="table" style="background: #033242;">
                <?php
                    $TableRows = 2;
                    $TableCols = 20;
                    $HTMLTable = "";
                    for ($i = 1; $i <= $TableRows; $i++) {
                        $HTMLTable .= "<tr>";
                        $Step = $TableRows+1-$i;
                        for ($x = 1; $x <= $TableCols; $x++) {
                            if($i == $TableRows) {
                                $TolltipPosition = 'bottom';
                            } else {
                                $TolltipPosition = 'top';
                            }
                            // Color-Down = #d33682
                            // Color-Up = #8a9d00
                            $HTMLTable .= "
                                <td style='font-size: 4px;' data-toggle='tooltip' data-placement='".$TolltipPosition."' title='GPIO ".$Step."'>
                                    <div style='border: 0px; background: #8a9d00; border-radius: 50%; width: 8px; height: 8px;'></div>
                                </td>
                            ";
                            $Step = $Step + $TableRows;
                        }
                        $HTMLTable .= "</tr>";
                    }
                    
                    echo $HTMLTable;
                ?>
            </table>
            <p>If you have installed RASPaccess with the official installer, you can load the board configuration here. We use <code>GPIO Reader</code> for this. Every GPIO will be checked, please be patient.</p>
            <button type="submit" class="btn btn-secondary float-right">Load Board</button>
          </div>
        </div>
    </div>
</div>
-->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit database access</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form id="database">
              <div class="form-group">
                <label for="dbserver">Server</label>
                <input type="text" name="dbserver" class="form-control" id="dbserver" placeholder="DB-Server" value="<?php echo $config->get('db','server');?>" disabled="disabled">
              </div>
              <div class="form-group">
                <label for="database">Database</label>
                <div class="input-group">
                  <input type="text" name="database" class="form-control" id="database" placeholder="Database Name" value="<?php echo $config->get('db','database');?>" disabled="disabled">
                  <span class="input-group-btn">
                    <button class="btn btn-danger" id="createdb" type="button" data-toggle='tooltip' data-placement='left' title='All data will be lost and database schema recreated!'>Create</button>
                  </span>
                </div>
                
              </div>
              <div class="form-group">
                <label for="dbuser">User</label>
                <input type="text" name="dbuser" class="form-control" id="dbuser" placeholder="DB-User" value="<?php echo $config->get('db','user');?>">
              </div>
              <div class="form-group">
                <label for="dbpassword">Password</label>
                <input type="password" name="dbpassword" class="form-control" id="dbpassword" placeholder="DB-Password" value="<?php echo $config->get('db','password');?>">
              </div>
              <button type="button" id="savedb" class="btn btn-secondary float-right">Save Configuration</button>
            </form>
      </div>
      <div class="modal-footer text-secondary">
        <small class="text-left"><i class="mdi mdi-alert-octagon"></i> You can also update the ini files manually.</small>
      </div>
    </div>
  </div>
</div>

<script>
    /* global $ */
    $('#changepw').on('click', function() {
      rajax('POST','application/api/v1/setup/adminpw','adminpw',true,true);
    });
    $('#savedb').on('click', function() {
      rajax('POST','application/api/v1/setup/database','database',false,false,'exampleModal');
    });
    $('#createdb').on('click', function() {
      rajax('POST','application/api/v1/setup/dbschema','',false,true,'exampleModal');
    });
    
</script>