<div class="header clearfix">
    <nav>
      <ul class="nav nav-pills float-right">
        <li class="nav-item">
          <a class="nav-link active" href="#">Sign in</a>
        </li>
      </ul>
    </nav>
    <h3 class="text-muted"><i class="mdi mdi-verified" style="color: #ec1354;"></i> RASPaccess</h3>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="card border-primary">
          <div class="card-header">
                Sign in
          </div>
          <div class="card-body" style="height: 190px;">
                <form id="loginform">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Admin Password">
                  </div>
                  <button type="button" class="btn btn-primary float-right" id="signin">I'm back!</button>
                </form>
          </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
                Instance State
          </div>
          <div class="card-body" style="height: 190px;">
            <table class="table">
                <tbody>
                    <?php if($SiteState['service_ok'] != "border-success") : ?>
                        <tr>
                          <th scope="row"><i class="mdi mdi-alert-circle text-danger"></i></th>
                          <td>Service not installed</td>
                        </tr>
                    <?php else : ?>
                        <tr>
                          <th scope="row"><i class="mdi mdi-check-circle text-success"></i></th>
                          <td>Service not installed</td>
                        </tr>
                    <?php endif ?>
                    
                    <?php if($SiteState['db_ok'] != "border-success") : ?>
                        <tr>
                          <th scope="row"><i class="mdi mdi-alert-circle text-danger"></i></th>
                          <td>Database is not ready</td>
                        </tr>
                    <?php else : ?>
                        <tr>
                          <th scope="row"><i class="mdi mdi-check-circle text-success"></i></th>
                          <td>Database is ready</td>
                        </tr>
                    <?php endif ?>
                    
                    <?php if($SiteState['server_ok'] != "border-success") : ?>
                        <tr>
                          <th scope="row"><i class="mdi mdi-alert-circle text-danger"></i></th>
                          <td>SERVaccess is not configurated</td>
                        </tr>
                    <?php else : ?>
                        <tr>
                          <th scope="row"><i class="mdi mdi-check-circle text-success"></i></th>
                          <td>SERVaccess is connected</td>
                        </tr>
                    <?php endif ?>
                    
                </tbody>
            </table>
          </div>
        </div>
    </div>
    
</div>
<script>
    /* global $ alertify */
    $('#exampleInputPassword1').focus();
    
        $(document).bind('keypress', function(e) {
        if(e.keyCode==13){
             $('#signin').trigger('click');
         }
    });
    
    $('#signin').on('click', function() {
      rajax('POST','application/api/v1/auth/signin','loginform',true,true);
    });
    
    
</script>