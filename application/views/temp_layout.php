
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./assets/img/favicon.ico">
    
    <title>RASPaccess</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <!-- Custom styles for core -->
    <link href="./assets/css/core.css" rel="stylesheet">
    <!-- Font CSS -->
    <link href="./assets/fonts/material/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />
    
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="./assets/js/datatables/datatables.min.css">
    <style>
      .page-link { font-size: 14px; margin-top: 10px;}
    </style>
  </head>

  <body>

    <div class="container">
        <?php echo @$header_content;?>
        <?php echo @$body_content;?>
     
        <footer class="footer" style="margin-top: 30px;">
            <p>created with <i class="mdi mdi-heart" style="color: #ec1354;"></i> by <a class="creator" href="mailto:wb9810@gmail.com">wb9810</a></p>
        </footer>

    </div> <!-- /container -->
    <script>
    
      $("[data-toggle=tooltip]").tooltip({container : 'body'});
      
      $("form").submit(function(e){
        e.preventDefault();
      });
      
      function rajax(type,url,form,empty=false,refresh=false,modalclose=false,table=false) {
        var data;
        if(form) {
          data = $( '#'+form ).serialize();
        } else {
          data = "";
        }
        
        entireurl = $(location).attr('protocol')+'//'+$(location).attr('hostname')+'/raspaccess/'+url;
        
        $.ajax({
            type: type,
            url: entireurl,
            data: data,
            success: function(msg){
                alertify.logPosition("top left");
                alertify.success( '<i class="mdi mdi-check-circle-outline"></i> ' + msg );
                if(refresh) {
                  setTimeout(function(){ window.location.reload(true); }, 1000);  
                }
                if(modalclose) {
                  $('#'+modalclose).modal('toggle');
                }
                if(table) {
                  $('#'+table).DataTable().ajax.reload();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alertify.logPosition("top left");
                alertify.error( '<i class="mdi mdi-alert-circle-outline"></i> ' + XMLHttpRequest.responseText );
                if(empty) {
                  $('#'+form).trigger("reset");
                  $("#"+form+":not(.filter) :input:visible:enabled:first").focus();
                }
            }
        });
      } 
      
      $.urlParam = function(name){
          var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
          if (results==null){
             return null;
          }
          else{
             return decodeURI(results[1]) || 0;
          }
      }
    </script>
      
    <!-- Alertify -->
    <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
    

  </body>
</html>
