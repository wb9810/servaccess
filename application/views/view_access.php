<table id="access_table" class="table table-hover table-striped">
    <thead>
        <tr>
            <th>RFID Tag</th>
            <th>Comment</th>
            <th>State</th>
        </tr>
    </thead>
</table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add new entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form id="accesscard">
              <div class="form-group">
                <label for="exampleInputPassword1">RFID Tag</label>
                <input type="text" name="rfidtag" class="form-control" id="exampleInputPassword1" placeholder="Scan or enter Tag">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword2">Comment</label>
                <input type="text" name="comment" class="form-control" id="exampleInputPassword2" placeholder="Put a comment as reminder">
              </div>
              <button type="button" id="createentry" class="btn btn-primary float-right">Save Entry</button>
            </form>
      </div>
      <div class="modal-footer text-secondary">
        <small class="text-left"><i class="mdi mdi-alert-octagon"></i> These entries are stored locally.</small>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ChangeRFID" tabindex="-1" role="dialog" aria-labelledby="ChangeRFIDLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ChangeRFIDLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form id="updateaccesscard">
              <input type="hidden" name="id" id="ChangeRFID_ID" />
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="ChangeRFID_State">State</label>
                    <select class="form-control" id="ChangeRFID_State" name="active">
                      <option value="1">active</option>
                      <option value="0">inactive</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword2">Remove Entry</label><br>
                    <button type="button" id="removeentry" class="btn btn-block btn-outline-danger" data-toggle='tooltip' data-placement='top' title='All history data will be deleted!'>Yes, remove!</button>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword2">Comment</label>
                <input type="text" name="comment" class="form-control" id="ChangeRFID_Comment" placeholder="Put a comment as reminder">
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword2">Created</label>
                    <input type="text" disabled="disabled" name="tempcreated" class="form-control" id="tempcreated">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword2">Updated</label>
                    <input type="text" disabled="disabled" name="tempupdated" class="form-control" id="tempupdated">
                  </div>
                </div>
              </div>
              <button type="button" id="updateentry" class="btn btn-primary float-right">Update Entry</button>
            </form>
      </div>
      <div class="modal-footer text-secondary">
        <small class="text-left"><i class="mdi mdi-alert-octagon"></i> These entry is stored locally.</small>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" charset="utf8" src="./assets/js/datatables/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script>
    /* global $ */
    $('#createentry').on('click', function() {
      rajax('POST','application/api/v1/access/add','accesscard',true,false,'exampleModal','access_table');
    });
    
    $('#updateentry').on('click', function() {
      rajax('POST','application/api/v1/access/update','updateaccesscard',true,false,'ChangeRFID','access_table');
    });
    
    $('#removeentry').on('click', function() {
      rajax('POST','application/api/v1/access/remove','updateaccesscard',true,false,'ChangeRFID','access_table');
    });
    
    $(document).ready( function () {
        var table = $('#access_table').DataTable({
            dom: "<'row'<'col-sm-3'B><'col-sm-3'l><'col-sm-6'f>>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            //dom: 'Bfltip',
            buttons: [
            {
                text: '<i class="mdi mdi-plus"></i> Add entry',
                className: 'btn-outline-primary btn-sm btn',
                action: function ( e, dt, node, config ) {
                  $('#exampleModal').modal('show');
                }
            }
            ],
            "ajax": $(location).attr('protocol')+'//'+$(location).attr('hostname')+'/raspaccess/application/api/v1/access/all',
            aoColumns: [
                { data: 'rfidtag',
                  render: function(data, type, row, meta){
                    return data+' <a href="'+$(location).attr('protocol')+'//'+$(location).attr('hostname')+'/raspaccess/application/logfile?rfid='+data+'"><i class="mdi mdi-history"></i></a> ';
                }},
                { data: 'comment' },
                { data: 'active',
                  render: function(data, type, row, meta){
                    if(data == true ) {
                      return '<span class="badge badge-success">active</span>';
                    } else {
                      return '<span class="badge badge-secondary">inactive</span>';
                    }
                }}
            ]
        });
        
        $('#access_table tbody').on('dblclick', 'tr', function () {
          var data = table.row( this ).data();
          $('#ChangeRFIDLabel').text('Tag #'+data.rfidtag);
          $('#ChangeRFID_ID').val(data.id);
          $('#ChangeRFID_State').val(data.active);
          $('#ChangeRFID_Comment').val(data.comment);
          $('#tempcreated').val(data.created);
          $('#tempupdated').val(data.updated);
          $('#ChangeRFID').modal('show');
          //console.log( table.row(this).data() );
        } );
        
        if( $.urlParam('create') ) {
          $('#exampleInputPassword1').val($.urlParam('create'));
          $('#exampleModal').modal('show');
          $('#exampleInputPassword2').focus();
        }
    } );
    
    
</script>