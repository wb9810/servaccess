<div class="row">
    <div class="col-sm-12 text-center text-secondary">
        <i class="mdi mdi-logout-variant text-secondary" style="font-size: 140px;"></i>
        <h3>Do you really want to log out?<br><br></h3>
        <button type="button" id="signout" class="btn btn-danger">Yes, take me out!</button>
    </div>
</div>

<script>
    $('#signout').on('click', function() {
        rajax('POST','https://raspaccess-wb9810-1.c9users.io/application/api/v1/auth/signout','',false,true);
    });
</script>