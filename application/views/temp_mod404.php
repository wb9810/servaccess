<div class="row">
    <div class="col-sm-12 text-center text-secondary">
        <i class="mdi mdi-package text-danger" style="font-size: 140px;"></i>
        <h1>Module not found!<br></h1>Go back to the <a href="./">Mainsite</a>.
    </div>
</div>